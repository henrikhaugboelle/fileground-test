tests = []

for file of window.__karma__.files
    if window.__karma__.files.hasOwnProperty(file)
        if /// \.test\.coffee$ ///.test(file)
            tests.push(file)

mocha.setup 'bdd'

requirejs.config {
    baseUrl: '/base',

    paths: {
        'fileground': 'http://localhost:3000/v1/fileground',
        'chai': 'node_modules/chai/chai',
        'underscore': 'node_modules/underscore/underscore'
    },

    shim: {
        'underscore': {
            exports: '_'
        }
    },

    deps: tests,

    callback: window.__karma__.start
}

require ['underscore'], (_) ->
    window._ = _

require ['chai'], (chai) ->
    window.should = chai.should()
    window.expect = chai.expect
    window.assert = chai.assert
