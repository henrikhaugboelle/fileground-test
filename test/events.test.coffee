define ['fileground'], (FileGround) ->
	BUCKET = 'http://localhost:3000'

	getRef = () ->
		new FileGround.Reference(BUCKET)

	describe 'FileGround', () ->
		describe 'Events', () ->

			it 'should contain no callbacks initially', () ->
				ref = getRef()

				ref.callbacks.should.be.empty

			it 'should be possible to register callbacks for arbitrary events', () ->
				ref = getRef()

				ref.on 'imAnEvent', () ->
					# callback
				ref.on 'iAmAnotherEvent', () ->
					# callback

				ref.callbacks['imAnEvent'].length.should.equal 1
				ref.callbacks['iAmAnotherEvent'].length.should.equal 1

			it 'should be possible to register multiple callbacks for same event', () ->
				ref = getRef()

				ref.on 'imAnEvent', () ->
					# callback
				ref.on 'imAnEvent', () ->
					# callback


				ref.callbacks['imAnEvent'].length.should.equal 2

			it 'should execute callbacks for arbitrary events', () ->
				ref = getRef()

				done = _.after 2, ->
					true.should.equal true

				ref.on 'imAnEvent', done
				ref.on 'iAmAnotherEvent', done

				ref.emit 'imAnEvent'
				ref.emit 'iAmAnotherEvent'

			it 'should execute multiple callbacks for same event', () ->
				ref = getRef()

				done = _.after 2, ->
					true.should.equal true

				ref.on 'imAnEvent', done
				ref.on 'imAnEvent', done

				ref.emit 'imAnEvent'

			it 'should execute callbacks with parameters', () ->
				ref = getRef()

				done = _.after 2, ->
					true.should.equal true

				ref.on 'imAnEvent', (a, b, c) ->
					a.should.equal 'a'
					b.should.equal 'b'
					c.should.equal 'c'

					done()

				ref.on 'iAmAnotherEvent', (d, e) ->
					d.should.equal 'd'
					e.should.equal 'e'

					done()

				ref.emit 'imAnEvent', ['a', 'b', 'c']
				ref.emit 'iAmAnotherEvent', ['d', 'e']